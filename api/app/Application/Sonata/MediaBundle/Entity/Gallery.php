<?php

/**

 */

namespace Application\Sonata\MediaBundle\Entity;

use Sonata\MediaBundle\Entity\BaseGallery as BaseGallery;
use Doctrine\ORM\Mapping as ORM;


/**
 * Gallery
 * 
 * @ORM\Table(name="media__gallery")
 * @ORM\Entity
 *  
 */
 
class Gallery extends BaseGallery
{
    /**
     * @var integer $id
	 * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")     
	 */
    protected $id;

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }
}
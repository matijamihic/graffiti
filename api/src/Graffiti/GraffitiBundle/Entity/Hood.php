<?php

namespace Graffiti\GraffitiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hood
 *
 * @ORM\Table(name="hood", uniqueConstraints={@ORM\UniqueConstraint(name="idHood_UNIQUE", columns={"id"})})
 * @ORM\Entity
 */
class Hood
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * Set name
     *
     * @param string $name
     * @return Hood
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}

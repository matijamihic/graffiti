<?php

namespace Graffiti\GraffitiBundle\Form\Graffiti;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Sonata\MediaBundle\Provider\ImageProvider;

class AddGalleryMedia extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
       		->setMethod('POST')
			// ->add('name', 'text')
			->add('media', 'sonata_media_type', array(
			     'provider' => 'sonata.media.provider.image',
			     'context'  => 'default'
			))
			->add('save', 'submit')
			->add('save_and_add', 'submit')
			->getForm();
	}
	public function getName()
    {
        return 'form';
    }
	
	
}

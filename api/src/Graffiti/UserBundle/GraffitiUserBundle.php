<?php

namespace Graffiti\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class GraffitiUserBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle';
	}
}

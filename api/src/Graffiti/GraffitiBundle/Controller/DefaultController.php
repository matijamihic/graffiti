<?php

namespace Graffiti\GraffitiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializationContext;

use Graffiti\GraffitiBundle\Entity\Author;
use Graffiti\GraffitiBundle\Entity\Graffiti;
use Graffiti\GraffitiBundle\Entity\Locations;

use Application\Sonata\MediaBundle\Entity\Gallery;
use Application\Sonata\MediaBundle\Entity\GalleryHasMedia;
use Application\Sonata\MediaBundle\Entity\Media;

use Graffiti\GraffitiBundle\Form\Graffiti\AddGraffiti;
use Graffiti\GraffitiBundle\Form\Graffiti\AddGalleryMedia;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

// these import the "@Route" and "@Template" annotations - nisam ovo jos pokusao implementirati
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/*
 * Default Controller contains all actions available to non-registred user.
 * 
 * Default Controller contains following actions:
 * 	- createGraffitiAction
 *  - displayGraffitiAction
 */
class DefaultController extends Controller
{
    /**
	 * 
	 * This method creats new graffiti object. First it renders form.
     * When form is submitted (isValid) it populates new graffiti object with
	 * that data and presists it to database.
     * Then it asks for more pictures(createGalleryMediaAction) or not(task_success), 
	 * depending on user action.
	 * 
	 * @ApiDoc(
	 * resource=true,
	 * description="Create new graffiti object",
	 * parameters={
	 *      {"name"="newGraffiti", "dataType"="object", "required"=true, "description"="new graffiti object that is presisted to db"},
	 * 		{"name"="form", "dataType"="form", "required"=true, "description"="AddGraffiti Form"},
	 * 		{"name"="gallery", "dataType"="object", "required"=true, "description"="new gallery object is created automaticly upon creation of new graffiti object, so every graffiti has dedicated gallery"}
	 *  }
     * )
	 * 
     */
    public function createGraffitiAction(Request $request)
    {
    	$graffiti = new Graffiti();
		$form = $this->createForm(new AddGraffiti(), $graffiti);
		$form->handleRequest($request);

	    if ($form->isValid())  //do this for valid submitted form:
	    {
	     	$data = $request->request->all();   //get request data form submitted form
			
			$locations = $this->getDoctrine()   //cretate locations object from submitted id
							->getRepository('GraffitiGraffitiBundle:Locations')		
							->find($data['form']['location']);
			$year = new \DateTime(date($data['form']['year']['year'] . "-01"));  //year needs to be datetime
		
			$gallery = new Gallery();  //create new gallery object
			$gallery->setContext('default');
			$gallery->setName($data['form']['name']);
			$gallery->setDefaultFormat('default_small');
			$gallery->setEnabled(true);
			
			$newGraffiti = new Graffiti();  //now when we have all data we need, create new graffiti object
			$newGraffiti->setLocation($locations);
			$newGraffiti->setName($data['form']['name']);
			$newGraffiti->setDescription($data['form']['description']);
			$newGraffiti->setYear($year);
			$newGraffiti->setImage($form->getData()->getImage());
			$newGraffiti->setGallery($gallery);
			foreach($data['form']['author'] as $authorId)
			{
				$author = $this->getDoctrine()
								->getRepository('GraffitiGraffitiBundle:Author')		
								->find($authorId);
				$newGraffiti->setAuthor($author);
			}

			$em = $this->getDoctrine()->getManager(); //presist to db
			$em->persist($newGraffiti);
			$em->flush();
						
			if ($form->get('save_and_add')->isClicked())  //redirect to add more images
			{				
				return $this->redirect($this->generateUrl('_create_gallerymedia', array(
				 	'id'=> $newGraffiti->getGallery()->getId() ) ));
			}
			
			return $this->redirect($this->generateUrl('_task_success'));	
	    }
		
		//render form
	    return $this->render('GraffitiGraffitiBundle:Default:createGraffiti.html.twig', array(
			'form' => $form->createView(),
		));
    }
    /**
	 * This mehtod creates new Media Object. It requires gallery_id of
	 * existing gallery.
	 * First it renders form for file imput.
	 * When form is submitted (isValid) it creates new Media object and presists it to db.
	 * Then it asks should more images be loaded (again this action) or not (task_success),
	 * depending od user action.
	 *
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Create new image object",
	 *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Id of gallery in which new image will be created"}
     *  },
	 *  parameters={
     *      {"name"="gallery", "dataType"="object", "required"=true, "description"="gets gallery object with gallery id"},
	 * 		{"name"="form", "dataType"="form", "required"=true, "description"="AddGalleryMedia Form"},
	 *      {"name"="galleryHasMedia", "dataType"="object", "required"=true, "description"="we create new image object via AddGalleryMedia object. look at Application/Sonata/MediaBundle/Entity"},
	 *  } 
     * )
     */
	public function createGalleryMediaAction(Request $request, $id)
	{		
		$newGalleryHasMedia = new GalleryHasMedia();  //here we create new GalleryHasMedia Object.
													  //in model you can see that that is place where all
													  //connection between images and galleries are stored
		$form = $this->createForm(new AddGalleryMedia(), $newGalleryHasMedia);  //display form
		$form->handleRequest($request);

	    if ($form->isValid())  //if form is valid and submited:
	    {
			$gallery = $this->getDoctrine()  //get gallery object by id
				->getRepository('ApplicationSonataMediaBundle:Gallery')		
				->findOneById($id);
			$newGalleryHasMedia->setGallery($gallery);
			$newGalleryHasMedia->setMedia($form->getData()->getMedia());
			$newGalleryHasMedia->prePersist();
     		
     		$em = $this->getDoctrine()->getManager();
			$em->persist($newGalleryHasMedia);
			$em->flush();
			
			if ($form->get('save_and_add')->isClicked())  // if save_and_add is clicked go to that actoion
			{
				return $this->redirect($this->generateUrl('_create_gallerymedia', array(
				 	'id'=> $id ) ));
			}
			
			return $this->redirect($this->generateUrl('_task_success')); //if everything is stored corectly
	    }																 //go to task_succes
		
		//render form
		 return $this->render('GraffitiGraffitiBundle:Default:createGalleryMedia.html.twig', array(
			'form' => $form->createView(),
		 ));
	}
    /**
	 * This mehtod gets single graffiti object by it's id.
	 * It can return html or json, depending on selected format.
	 * If graffiti with selected id is not accesible or not existing, it redirects to listGraffiti
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Display graffiti",
	 *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Id of graffiti to display"},
	 * 		{"name"="format", "dataType"="integer", "required"="false", "description"="html or json"}
     *  },
	 *  parameters={
     *      {"name"="graffiti", "dataType"="object", "required"=true, "description"="graffiti object"},
	 *  } 
     * )
     */
	public function displayGraffitiAction($id, $format)
	{
		$graffiti = $this->getDoctrine() //get graffiti object from db
					->getRepository('GraffitiGraffitiBundle:Graffiti')		
					->find($id);
					
		if ($graffiti && $graffiti->getDisplay() == 'true')  //check for display==true
		{			
			if ($format == 'json') //check if format is .json
			{
				$serializer = $this->get('jms_serializer'); //get serializer
				//serialize graffiti object into json
				$json = $serializer->serialize($graffiti, 'json', SerializationContext::create()->enableMaxDepthChecks());

				$data = json_decode($json, true); //decode json
				$main_image_urls=$this->getImageLinksAction($graffiti->getImage()); //and needed urls
				$data['image']['url']=$main_image_urls;
				$data['gallery']['url']=$this->getGalleryLinksAction($data['gallery']['id']);
				$json = json_encode($data); //encode back to json
				
				$response = new Response();
				$response->setContent($json);
				$response->headers->set('Content-Type', 'application/json');
				
				return $response;		
			}
			else //if format is html return view
			{
				return $this->render('GraffitiGraffitiBundle:Default:displayGraffiti.html.twig', array(
					'graffiti' => $graffiti
				));				
			}
		}
		else // if there is no graffiti with that id, or it is not enabled then redirect to list/graffiti
		{
			return $this->redirect($this->generateUrl('_list_graffiti'));
		}
	}
    /**
	 * This mehtod gets single author object by it's id.
	 * It can return html or json, depending on selected format.
	 * If graffiti with selected id is not accesible or not existing, it redirects to listAuthors
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Display author",
	 *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Id of graffiti to display"},
	 * 		{"name"="format", "dataType"="integer", "required"="false", "description"="html or json"}
     *  },
	 *  parameters={
     *      {"name"="author", "dataType"="object", "required"=true, "description"="author object"},
	 *  } 
     * )
     */	
	public function displayAuthorAction($id, $format)
	{
		$author = $this->getDoctrine() //get author object
					->getRepository('GraffitiGraffitiBundle:Author')		
					->find($id);	
					
		if ($author) //if we have author object
		{
			if($format == 'json')  //check if fortat is json
			{
				$serializer = $this->get('jms_serializer');  //get serializer
				//serialize author object into json
				$json = $serializer->serialize($author, 'json', SerializationContext::create()->enableMaxDepthChecks());
				
				$data = json_decode($json, true); //decode into array
				$data['image']['url']=$this->getImageLinksAction($author->getImage()); //add needed urls
				foreach($data['graffiti'] as &$graffiti)
				{
					$image=$this->getImageByIdAction($graffiti['image']['id']);
					$graffiti['image']['url']=$this->getImageLinksAction($image);
				}
				$json = json_encode($data); //encode back to json
				
				$response = new Response();
				$response->setContent($json);
				$response->headers->set('Content-Type', 'application/json');
				
				return $response;		
			}
			else //if format is html return view
			{	
				return $this->render('GraffitiGraffitiBundle:Default:displayAuthor.html.twig', array(
					'author' => $author,
				));
			}
		}	
		else //if there is no such author then redirect to /authors/list
		{
			return $this->redirect($this->generateUrl('_list_authors'));
		}
	}

    /**
	 * This mehtod gets lists all graffiti objects.
	 * It can return html or json, depending on selected format.
     *
     * @ApiDoc(
     *  resource=true,
     *  description="List graffitis",
	 *  requirements={
	 * 		{"name"="format", "dataType"="integer", "required"="false", "description"="html or json"}
     *  },
	 *  parameters={
     *      {"name"="graffitis", "dataType"="object", "required"=true, "description"="collection of graffiti objects"},
	 *  } 
     * )
     */
	public function listGraffitiAction($format)
	{
		$graffitis = $this->getDoctrine()  //get all graffiti objects with display==1
						->getRepository('GraffitiGraffitiBundle:Graffiti')
            			->findByDisplay('1');
		
		if ($format == 'json') //check if formt is json
			{
				$serializer = $this->get('jms_serializer');
				$json = $serializer->serialize($graffitis, 'json', SerializationContext::create()
															->enableMaxDepthChecks()
															->setGroups(array('list'))
															);
				$data = json_decode($json, true); //decode json to array
				foreach($data as &$listItem) //add needed links to json
				{
					$graffiti = $this->getDoctrine()
						->getRepository('GraffitiGraffitiBundle:Graffiti')
            			->findOneById($listItem['id']);
						
					$image=$graffiti->getImage();
					$listItem['image']['url']=$this->getImageLinksAction($image);
				}
				$json = json_encode($data); //encode back
				
				$response = new Response();
				$response->setContent($json);
				$response->headers->set('Content-Type', 'application/json');
				
				return $response;
			}
			else
			{
				return $this->render('GraffitiGraffitiBundle:Default:listGraffiti.html.twig', array(
					'graffitis' => $graffitis
				));
			}
	}
    /**
	 * This mehtod gets lists all author objects.
	 * It can return html or json, depending on selected format.
     *
     * @ApiDoc(
     *  resource=true,
     *  description="List authors",
	 *  requirements={
	 * 		{"name"="format", "dataType"="integer", "required"="false", "description"="html or json"}
     *  },
	 *  parameters={
     *      {"name"="authors", "dataType"="object", "required"=true, "description"="collection of author objects"},
	 *  } 
     * )
     */
	public function listAuthorsAction($format)
	{
		$authors = $this->getDoctrine()
					->getRepository('GraffitiGraffitiBundle:Author')
					->findAll();
		
		if ($format == 'json')  // checks for format paramter in link (.../author.json)
		{
			$serializer = $this->get('jms_serializer');   //get serializer
			//serialize authors object
			$json = $serializer->serialize($authors, 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups(array('list')));
				
			$data = json_decode($json, true);  //deseiralize
			foreach($data as &$listItem)  // make changes to desirialized json
			{
				$author = $this->getDoctrine()
					->getRepository('GraffitiGraffitiBundle:Author')
        			->findOneById($listItem['id']);
					
				$image=$author->getImage();
				if ($image)	{
					$listItem['image']['url']=$this->getImageLinksAction($image);
				}
			}
			$json = json_encode($data);  //serialize back
				
			$response = new Response();  //response
			$response->setContent($json);
			$response->headers->set('Content-Type', 'application/json');
			
			return $response;	
		}
		else  //if format is null or html then show html:
		{
			return $this->render('GraffitiGraffitiBundle:Default:listAuthors.html.twig', array(
				'authors' => $authors
			));
		}
	}
    /**
	 * This mehtod gets links for particular image object
	 * It returns array of urls.
     * This method should be moved out of controller in feauture.
	 * It is here only becaouse this fucncionality was recently added
	 * 
     */	
	public function getImageLinksAction($image)
	{
 		$provider = $this->container->get($image->getProviderName());  //get provider
		$formats = $this->get('sonata.media.pool')->getFormatNamesByContext($image->getContext()); //get image formats(can be changed in main confing.yml)

		foreach($formats as $key => $format)  //get url for each format
		{
			$url[$key] = $provider->generatePublicUrl($image, $key);
		}
		
		return $url;  //return array of urls for particular image
	}
	/**
	 * This method gets image object by image id
     * This method should be moved out of controller in feauture.
	 * It is here only becaouse it was needed for fucncionality that was recently added.
	 * 
     */	
	public function getImageByIdAction($id)
	{
		$image = $this->getDoctrine()
			->getRepository('ApplicationSonataMediaBundle:Media')
			->findOneById($id);
		
		return $image;
	}
    /**
	 * This mehtod gets links for particular gallery object
	 * It returns array of urls for all media in that gallery.
     * This method should be moved out of controller in feauture.
	 * It is here only becaouse this fucncionality was recently added
	 * 
     */	
	public function getGalleryLinksAction($galleryId)
	{
		$gallery = $this->getDoctrine()  //get gallery by id
			->getRepository('ApplicationSonataMediaBundle:Gallery')
			->findOneById($galleryId);
		foreach ($gallery->getGalleryHasMedias() as $media)  //for all images in gallery call getImageLinks and
		{													 //store data in a array
			$image = $media->getMedia();
			$url[$image->getId()]=$this->getImageLinksAction($image);
		}
		return $url;  //return array of links to images
	}
}
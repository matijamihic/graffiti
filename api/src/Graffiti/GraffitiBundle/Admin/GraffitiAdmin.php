<?php
namespace Graffiti\GraffitiBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class GraffitiAdmin extends Admin
{
	    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
    	//$link parameters array is filled with data necessary to display sonata_type_model_list type.
    	// this whole part of code is reused form 3rd party sonata-project/media-bundle/admin
    	$link_parameters = array();

        if ($this->hasParentFieldDescription()) {
            $link_parameters = $this->getParentFieldDescription()->getOption('link_parameters', array());
        }

        if ($this->hasRequest()) {
            $context = $this->getRequest()->get('context', null);

            if (null !== $context) {
                $link_parameters['context'] = $context;
            }
        }
        $formMapper
            ->add('name', 'text', array('label' => 'Graffiti name'))
			->add('location', 'entity', array(
				'class' => 'Graffiti\GraffitiBundle\Entity\Locations',
				'property' => 'name'))
			->add('year', 'date')
			->add('description', 'textarea', array(
				'label' => 'Description',
				'required'=>false))
			->add('author', 'sonata_type_model', array(
				'expanded' => false, 
				'by_reference' => false, 
				'multiple' => true,
				'class' => 'GraffitiGraffitiBundle:Author',
				'property' => 'id',
				'required'=>false))
			->add('display', 'checkbox', array(
				'label' => 'Display',
				'required'=>false))
            ->add('image', 'sonata_type_model_list', array('required' => false), array(
                'link_parameters' => $link_parameters
            ))
			->add('gallery', 'sonata_type_model_list', array('required' => false), array(
                'link_parameters' => $link_parameters
            ))
        ;
    }
	
	    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
			->add('location.id')
			->add('location.name')
			->add('location.hood.name')
		    ->add('author.name')
		    ->add('author.id')
			->add('year')
			->add('description')
			->add('display')
        ;
	//	var_dump($datagridMapper);
    }
	    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
			->add('location.name')
			->add('location.id')
			->add('location.hood.name')
			->add('year')
			->add('description')
			->add('image')
			->add('gallery')
			->add('display')
        ;
    }
}

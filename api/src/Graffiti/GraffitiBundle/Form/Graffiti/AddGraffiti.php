<?php

namespace Graffiti\GraffitiBundle\Form\Graffiti;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Sonata\MediaBundle\Provider\ImageProvider;

class AddGraffiti extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
       		->setMethod('POST')
			->add('name', 'text')
			->add('year', 'date')
			->add('description', 'textarea', array(
				'required'=>false,
				'max_length' => 445
			))
			->add('location', 'entity', array(
				'class' => 'GraffitiGraffitiBundle:Locations',
				'property' => 'name'
			))
			->add('author', 'entity', array(
				'class' => 'GraffitiGraffitiBundle:Author',
				'property' => 'name',
				'expanded' => false, 
				'by_reference' => false, 
				'multiple' => true,
			))
			->add('image', 'sonata_media_type', array(
			     'provider' => 'sonata.media.provider.image',
			     'context'  => 'default'
			))
			->add('save', 'submit')
			->add('save_and_add', 'submit')
			->getForm();
	}
	public function getName()
    {
        return 'form';
    }
}

<?php

namespace Graffiti\GraffitiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Locations
 *
 * @ORM\Table(name="locations", uniqueConstraints={@ORM\UniqueConstraint(name="idLocations_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="idHood_idx", columns={"hood"})})
 * @ORM\Entity
 */
class Locations
{
	/**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=100, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=445, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="streetViewLink", type="string", length=145, nullable=true)
     */
    private $streetviewlink;

    /**
     * @var string
     *
     * @ORM\Column(name="geolocation", type="string", length=145, nullable=true)
     */
    private $geolocation;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Graffiti\GraffitiBundle\Entity\Hood
     *
     * @ORM\ManyToOne(targetEntity="Graffiti\GraffitiBundle\Entity\Hood")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hood", referencedColumnName="id")
     * })
     */
    private $hood;

    /**
     * Set address
     *
     * @param string $address
     * @return Locations
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Locations
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set streetviewlink
     *
     * @param string $streetviewlink
     * @return Locations
     */
    public function setStreetviewlink($streetviewlink)
    {
        $this->streetviewlink = $streetviewlink;

        return $this;
    }

    /**
     * Get streetviewlink
     *
     * @return string 
     */
    public function getStreetviewlink()
    {
        return $this->streetviewlink;
    }

    /**
     * Set geolocation
     *
     * @param string $geolocation
     * @return Locations
     */
    public function setGeolocation($geolocation)
    {
        $this->geolocation = $geolocation;

        return $this;
    }

    /**
     * Get geolocation
     *
     * @return string 
     */
    public function getGeolocation()
    {
        return $this->geolocation;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hood
     *
     * @param \Graffiti\GraffitiBundle\Entity\Hood $hood
     * @return Locations
     */
    public function setHood(\Graffiti\GraffitiBundle\Entity\Hood $hood = null)
    {
        $this->hood = $hood;

        return $this;
    }

    /**
     * Get hood
     *
     * @return \Graffiti\GraffitiBundle\Entity\Hood 
     */
    public function getHood()
    {
        return $this->hood;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Locations
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}

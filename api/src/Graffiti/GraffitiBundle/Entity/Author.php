<?php

namespace Graffiti\GraffitiBundle\Entity;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

use Doctrine\ORM\Mapping as ORM;

/**
 * Author
 *
 * @ORM\Table(name="author", uniqueConstraints={@ORM\UniqueConstraint(name="idAuthor_UNIQUE", columns={"id"})})
 * @ORM\Entity
 */
class Author
{
    /**
     * @var string
	 * 
	 * @Groups({"list"})
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var string
	 * 
	 * @Groups({"list"})
     *
     * @ORM\Column(name="nickname", type="string", length=45, nullable=true)
     */
    private $nickname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=145, nullable=true)
     */
    private $email;

    /**
     * @var integer
	 * 
	 * @Groups({"list"})
     *
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="image", referencedColumnName="id")
     */
    private $image;

    /**
     * @var string
	 * 
     * @ORM\Column(name="description", type="string", length=445, nullable=true)
     */
    private $description;

    /**
     * @var integer
	 * 
	 * @Groups({"list"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
	 * 
     *
     * @ORM\ManyToMany(targetEntity="Graffiti\GraffitiBundle\Entity\Graffiti", mappedBy="author")
     */
    private $graffiti;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->graffiti = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set name
     *
     * @param string $name
     * @return Author
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     * @return Author
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname
     *
     * @return string 
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Author
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set image
     *
     * @param integer $image
     * @return Author
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return integer 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Author
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add graffiti
     *
     * @param \Graffiti\GraffitiBundle\Entity\Graffiti $graffiti
     * @return Author
     */
    public function addGraffiti(\Graffiti\GraffitiBundle\Entity\Graffiti $graffiti)
    {
        $this->graffiti[] = $graffiti;

        return $this;
    }

    /**
     * Remove graffiti
     *
     * @param \Graffiti\GraffitiBundle\Entity\Graffiti $graffiti
     */
    public function removeGraffiti(\Graffiti\GraffitiBundle\Entity\Graffiti $graffiti)
    {
        $this->graffiti->removeElement($graffiti);
    }

    /**
     * Get graffiti
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGraffiti()
    {
        return $this->graffiti;
    }
}

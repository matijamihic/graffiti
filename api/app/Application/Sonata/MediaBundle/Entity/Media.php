<?php

/**

 */

namespace Application\Sonata\MediaBundle\Entity;

use Sonata\MediaBundle\Model\Media as MediaModel;
use Doctrine\ORM\Mapping as ORM;

/**
 * Gallery
 * 
 * 
 * @ORM\Table(name="media__media")
 * @ORM\Entity
 */
class Media extends MediaModel
{
    /**
     * @var integer $id
	 * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")  
     */
    protected $id;
    /**
     * @var string $name
	 * 
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 * 
     */
     protected $name;
	 
	/**
     * @var text $description
	 * 
	 * @ORM\Column(name="description", type="text", length=1024, nullable=true)
	 * 
     */
    protected $description;
	
	/**
     * @var boolean $enabled
     * 
	 * @ORM\Column(name="enabled", type="boolean", nullable=false)
	 * 
	 */
	 protected $enabled = false;
	/**
	 * @var string $provider_name
	 * 
	 * 
	 * @ORM\Column(name="provider_name", type="string", length=255, nullable=false)
	 *
     */
    protected $providerName;

    /**
     * @var integer $provider_status
	 * 
	 * @ORM\Column(name="provider_status", type="integer", nullable=false)
	 * 
	 * 
     */
    protected $providerStatus;

    /**
     * @var string $provider_reference
	 * 
	 * @ORM\Column(name="provider_reference", type="string", length=255, nullable=false)
	 * 
     */
    protected $providerReference;

    /**
     * @var array $provider_metadata
	 * 
	 * @ORM\Column(name="provider_metadata", type="json", nullable=true)
	 * 
     */
    protected $providerMetadata = array();

    /**
     * @var integer $width
	 * 
	 * @ORM\Column(name="width", type="integer", nullable=true)
	 * 
     */
    protected $width;

    /**
     * @var integer $height
	 * 
	 * @ORM\Column(name="height", type="integer", nullable=true)
	 * 
     */
    protected $height;

    /**
     * @var decimal $length
	 * 
	 * @ORM\Column(name="length", type="decimal", nullable=true)
	 * 
     */
    protected $length;

    /**
     * @var string $copyright
	 * 
	 * @ORM\Column(name="copyright", type="string", nullable=true)
	 * 
     */
    protected $copyright;

    /**
     * @var string $author_name
	 * 
	 * @ORM\Column(name="author_name", type="string", nullable=true)
	 * 
     */
    protected $authorName;

    /**
     * @var string $context
	 * 
	 * @ORM\Column(name="context", type="string", length=64, nullable=true)
	 * 
	 * 
     */
    protected $context;

    /**
     * @var boolean $cdn_is_flushable
	 * 
	 * @ORM\Column(name="cdn_is_flushable", type="boolean", nullable=true)
	 * 
	 * 
     */
    protected $cdnIsFlushable=false;

    /**
     * @var datetime $cdn_flush_at
	 * 
	 * @ORM\Column(name="cdn_flush_at", type="datetime", nullable=true)
	 * 
     */
    protected $cdnFlushAt;

    /**
     * @var integer $cdn_status
	 * 
	 * @ORM\Column(name="cdn_status", type="integer", nullable=true)
	 * 
     */
    protected $cdnStatus;

    /**
     * @var datetime $updated_at
	 * 
	 * @ORM\Column(name="updated_at", type="datetime")
	 * 
     */
    protected $updatedAt;

    /**
     * @var datetime $created_at
	 * 
	 * @ORM\Column(name="created_at", type="datetime")
	 * 
	 * 
     */
    protected $createdAt;

    protected $binaryContent;

    protected $previousProviderReference;

    /**
     * @var varchar $content_type
	 * 
	 * @ORM\Column(name="content_type", type="string", length=64, nullable=true)
	 * 
	 * 
     */
    protected $contentType;

    /**
     * @var integer $size
	 * 
	 * @ORM\Column(name="content_size", type="integer", nullable=true)
	 *
	 * 
     */
    protected $size;

    protected $galleryHasMedias;
	
    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }
	    
	public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
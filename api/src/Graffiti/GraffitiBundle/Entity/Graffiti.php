<?php

namespace Graffiti\GraffitiBundle\Entity;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

use Doctrine\ORM\Mapping as ORM;

/**
 * Graffiti
 *
 * @ExclusionPolicy("all")
 * 
 * @ORM\Table(name="graffiti", uniqueConstraints={@ORM\UniqueConstraint(name="idGraffiti_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="idLocations_idx", columns={"location"})})
 * @ORM\Entity(repositoryClass="Graffiti\GraffitiBundle\Entity\GraffitiRepository")
 */
class Graffiti
{
    /**
     * @var string
	 * 
     * @Expose
	 * @Groups({"list"})
	 * 
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
	 * 
	 * @Expose
     *
     * @ORM\Column(name="year", type="date", nullable=true)
     */
    private $year;

    /**
     * @var string
	 * 
	 * @Expose
     *
     * @ORM\Column(name="description", type="string", length=445, nullable=true)
     */
    private $description;

    /**
     * @var integer
	 * 
	 * @Groups({"list"})
	 * @Expose
	 * 
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="image", referencedColumnName="id")
	 *
	 */    
 	private $image;

    /**
     * @var boolean
	 * @Groups({"list"})
	 * @Expose
     *
     * @ORM\Column(name="display", type="boolean", nullable=true)
     */
    private $display;

    /**
     * @var integer
	 * 
	 * @Expose
     *
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\MediaBundle\Entity\Gallery", cascade={"persist"})
     * @ORM\JoinColumn(name="gallery", referencedColumnName="id")
     */
    private $gallery;

    /**
     * @var integer
	 * @Groups({"list"})
	 * @Expose
	 * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Graffiti\GraffitiBundle\Entity\Locations
	 * 
	 * @Groups({"list"})
	 * @Expose
     *
     * @ORM\ManyToOne(targetEntity="Graffiti\GraffitiBundle\Entity\Locations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="location", referencedColumnName="id")
     * })
     */
    private $location;

    /**
     * @var \Doctrine\Common\Collections\Collection
	 * 
	 * @Groups({"list"})
	 * @Expose
	 * @MaxDepth(2)
     *
     * @ORM\ManyToMany(targetEntity="Graffiti\GraffitiBundle\Entity\Author", inversedBy="graffiti")
     */
    private $author;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->author = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set name
     *
     * @param string $name
     * @return Graffiti
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set year
     *
     * @param \DateTime $year
     * @return Graffiti
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return \DateTime 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Graffiti
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param integer $image
     * @return Graffiti
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
	 * 
     *
     * @return integer 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set display
     *
     * @param boolean $display
     * @return Graffiti
     */
    public function setDisplay($display)
    {
        $this->display = $display;

        return $this;
    }

    /**
     * Get display
	 * 
     *
     * @return boolean 
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * Set gallery
     *
     * @param integer $gallery
     * @return Graffiti
     */
    public function setGallery($gallery)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
	 * 
     * @return integer 
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set location
	 * 
	 * 
     *
     * @param \Graffiti\GraffitiBundle\Entity\Locations $location
     * @return Graffiti
     */
    public function setLocation(\Graffiti\GraffitiBundle\Entity\Locations $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
	 * 
     *
     * @return \Graffiti\GraffitiBundle\Entity\Locations 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Add author
     *
     * @param \Graffiti\GraffitiBundle\Entity\Author $author
     * @return Graffiti
     */
    public function addAuthor(\Graffiti\GraffitiBundle\Entity\Author $author)
    {
        $this->author[] = $author;

        return $this;
    }
    /**
     * Set author
     *
     * @param \Graffiti\GraffitiBundle\Entity\Author $author
     * @return Graffiti
     */
    public function setAuthor(\Graffiti\GraffitiBundle\Entity\Author $author)
    {
        $this->author[] = $author;

        return $this;
    }
    /**
     * Remove author
     *
     * @param \Graffiti\GraffitiBundle\Entity\Author $author
     */
    public function removeAuthor(\Graffiti\GraffitiBundle\Entity\Author $author)
    {
        $this->author->removeElement($author);
    }

    /**
     * Get author
	 * 
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAuthor()
    {
        return $this->author;
    }
/**
	 * @Groups({"list"})
 	 * @VirtualProperty
     * @SerializedName("ImagePath")
     *
     * @return string
 * 
 */
	public function getMediaPublicUrl()
	{
	
	}
}

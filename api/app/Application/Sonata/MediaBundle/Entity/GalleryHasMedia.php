<?php

/**

 */

namespace Application\Sonata\MediaBundle\Entity;

use Sonata\MediaBundle\Model\GalleryHasMedia as GalleryHasMediaModel;
use Doctrine\ORM\Mapping as ORM;

/**
 * GalleryHasMedia
 * 
 * @ORM\Table(name="media__gallery__media")
 * @ORM\Entity
 */
class GalleryHasMedia extends GalleryHasMediaModel
{
    /**
     * @var integer $id
	 * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")  
	 * 
     */
    protected $id;
	
	protected $media;

    protected $gallery;
	
	/** 
	 * @var integer $position
	 * 
     * @ORM\Column(name="position", type="integer")
	 * 
	 */
    protected $position;
    /**
     * @var datetime $updated_at
	 * 
	 * @ORM\Column(name="updated_at", type="datetime")
	 * 
     */
    protected $updatedAt;
	
	/**
     * @var datetime $created_at
	 * 
	 * @ORM\Column(name="created_at", type="datetime")
	 * 
	 * 
     */
    protected $createdAt;
	/**
     * @var boolean $enabled
     * 
	 * @ORM\Column(name="enabled", type="boolean")
	 * 
	 */
    protected $enabled;

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }
	public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
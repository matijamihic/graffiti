<?php
namespace Graffiti\GraffitiBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class LocationsAdmin extends Admin
{
	    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        	->add('name', 'text', array('label' => 'Name'))
            ->add('address', 'text', array('label' => 'Address'))
			->add('hood', 'entity', array(
				'class' => 'Graffiti\GraffitiBundle\Entity\Hood',
				'property' => 'name'))
			->add('description', 'textarea', array(
				'label' => 'Description',
				'required'=>false))
			->add('street_view_link', 'text', array(
				'label' => 'StreetView Link',
				'required'=>false))
			->add('geolocation', 'text', array(	
				'label' => 'Geolocation',
				'required'=>false))
        ;
    }
	
	    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('name')
            ->add('address')
		    ->add('hood.name')
        ;
    }
	    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        	->addIdentifier('name')
            ->add('address')
		    ->add('hood.name')
			->add('description')
			->add('geolocation')
			->add('streetviewlink')
        ;
    }
}
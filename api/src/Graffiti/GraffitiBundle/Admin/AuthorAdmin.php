<?php
namespace Graffiti\GraffitiBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AuthorAdmin extends Admin
{
	    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
    	//$link parameters array is filled with data necessary to display sonata_type_model_list type.
    	// this whole part of code is reused form 3rd party sonata-project/media-bundle/admin
    	$link_parameters = array();
        
        if ($this->hasParentFieldDescription()) {
            $link_parameters = $this->getParentFieldDescription()->getOption('link_parameters', array());
        }

        if ($this->hasRequest()) {
            $context = $this->getRequest()->get('context', null);

            if (null !== $context) {
                $link_parameters['context'] = $context;
            }
        }
        $formMapper
            ->add('name', 'text', array(
            	'label' => 'Name'
				))
			->add('nickname', 'text', array(
				'label' => 'Nickname',
				'required'=>false
				))
			->add('email', 'text', array(
				'label' => 'Email',
				'required'=>false
				))
			->add('description', 'text', array(
				'label' => 'Description',
				'required'=>false
				))
			->add('image', 'sonata_type_model_list', array('required' => false), array(
                'link_parameters' => $link_parameters
            	))
        ;
    }
	
	    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
			->add('nickname')
			->add('email')
			->add('description')
        ;
    }
	    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        	->addIdentifier('id')
            ->addIdentifier('name')
			->add('nickname')
			->add('email')
			->add('description')
			->add('image')
        ;
    }
}
